package cl.ubb.determinarPrimo;

import static org.junit.Assert.*;

import static org.hamcrest.CoreMatchers.is;

import org.junit.Test;

public class DeterminarPrimoTest {

	@Test
	public void IngresarCeroRetornarPrimoFalso() {
		/*arrange*/
		NumeroPrimo primo = new NumeroPrimo();
		boolean resultado;
		
		/*act*/
		resultado = primo.DeterminarPrimo(0);
		
		/*assert*/
		assertThat(resultado, is(false));
	}

}
